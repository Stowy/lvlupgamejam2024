using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    [SerializeField] private Transform _camera; // set this via inspector
    float _shake = 0.0f;
    const float _shakeAmount = 0.075f;
    const float _decreaseFactor = 1.0f;
    bool isScreeshaking = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isScreeshaking)
            return;
        if (_shake > 0)
        {
            _camera.localPosition = Random.insideUnitSphere * _shakeAmount;
            _shake -= Time.deltaTime * _decreaseFactor;
        }
        else
        {
            _shake = 0.0f;
            isScreeshaking = false;
        }
    }

    public void Screenshake(float time_to_screenshake = 0.5f)
    {
        isScreeshaking = true;
        _shake = time_to_screenshake;
    }

}
