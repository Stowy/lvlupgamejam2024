﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Player
{
    public class CameraController : MonoBehaviour
    {
        [FormerlySerializedAs("m_PlayerTransform")] [SerializeField]
        private Transform _playerTransform;

        [FormerlySerializedAs("m_LookSpeed")] [SerializeField]
        private float _mLookSpeed = 9;

        [FormerlySerializedAs("m_LookMaxAngle")] [SerializeField]
        private float _mLookMaxAngle = 89f;

        
        private Vector2 _mouseLookInput;
        private float _xRotation;
        private GameManager _gameManager;

        private void Awake()
        {
            GameObject gameManager = GameObject.Find("GameManager");
            if (gameManager)
            {
                _gameManager = gameManager.GetComponent<GameManager>();
            }
        }


        private void Update()
        {
            if (_gameManager && _gameManager.IsInPause) return;

            Vector2 mouseDelta = Mouse.current.delta.value * 0.05f;
            float lookX = mouseDelta.x * _mLookSpeed;
            float lookY = mouseDelta.y * _mLookSpeed;
            _xRotation -= lookY;
            _xRotation = Mathf.Clamp(_xRotation, -_mLookMaxAngle, _mLookMaxAngle);

            transform.localRotation = Quaternion.Euler(_xRotation, 0, 0);
            _playerTransform.Rotate(Vector3.up * lookX);
        }
      
        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void OnApplicationFocus(bool hasFocus)
        {
            if (hasFocus && _gameManager == null)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }
    }
}