using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

namespace Player
{
    public class PlayerInputBehaviour : MonoBehaviour
    {
        [FormerlySerializedAs("m_Speed")] [SerializeField]
        private float _speed = 10;

        [SerializeField] private float _jumpHeight = 1;
        [SerializeField] private float _gravityValue = -9.81f;

        private CharacterController _characterController;

        private Vector2 _movementInput;
        private bool _playerShouldJump;
        private Vector3 _velocity;

        private float _currentSpeed = 0;
        private int _jumpStreak = 1;
        private float _bunnyHopClock = 0.0f;
        private bool _isGrounderFrFr = false;

        [SerializeField] private float _bHopAccelerationFactor = 1.1f;
        [SerializeField] private float _bunnyHopJumpWindow = 0.16f;
        [SerializeField] private float _bunnyHopDecayTime = 0.50f;
        [SerializeField] private int _stopJumpStreakAt = 5;

        private SoundManager _soundManager;
        private GameManager _gameManager;

        private void Awake()
        {
            _characterController = GetComponent<CharacterController>();
            _currentSpeed = _speed;

            GameObject soundManager = GameObject.Find("SoundManager");
            if (soundManager)
            {
                _soundManager = soundManager.GetComponent<SoundManager>();
            }
        }

        private void Start()
        {
            _gameManager = FindAnyObjectByType<GameManager>();
        }

        private void Update()
        {
            if (_gameManager.IsInPause) return;

            bool isPlayerGrounded = _characterController.isGrounded;
            if (isPlayerGrounded && _velocity.y < 0)
            {
                _velocity.y = 0f;
                _isGrounderFrFr = true;
            }

            var movement = new Vector3
            {
                x = _movementInput.x,
                z = _movementInput.y
            };
            movement = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0) * movement.normalized;
            _characterController.Move(movement * (_currentSpeed * Time.deltaTime));
            if (_isGrounderFrFr)
            {
                _bunnyHopClock += Time.deltaTime;
            }

            if (_playerShouldJump && isPlayerGrounded)
            {
                _isGrounderFrFr = false;
                _velocity.y = Mathf.Sqrt(_jumpHeight * -3.0f * _gravityValue);
                if (_bunnyHopClock < _bunnyHopJumpWindow)
                {
                    if (_jumpStreak <= _stopJumpStreakAt)
                    {
                        _jumpStreak++;
                        _currentSpeed = _speed + (_jumpStreak * _bHopAccelerationFactor);
                    }
                }

                _bunnyHopClock = 0;
                if (_soundManager)
                    _soundManager.PlaySound(SoundManager.Sound.Jump);
            }

            if (_isGrounderFrFr && _jumpStreak > 0 && _bunnyHopClock > _bunnyHopDecayTime)
            {
                _jumpStreak--;
                _currentSpeed = _speed + (_jumpStreak * _bHopAccelerationFactor);
                _bunnyHopClock = 0;
            }

            _velocity.y += _gravityValue * Time.deltaTime;
            _characterController.Move(_velocity * Time.deltaTime);
        }

        private void OnJump(InputValue value)
        {
            _playerShouldJump = value.Get<float>() > 0.5f;
        }

        private void OnMove(InputValue value)
        {
            _movementInput = value.Get<Vector2>();
        }

        public void OnPause(InputValue value)
        {
            if (_gameManager == null) return;
            
            if (GameObject.Find("NewGame"))
                return;
            
            _gameManager.OnPause();
        }
    }
}