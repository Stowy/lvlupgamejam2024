﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Player
{
    public class PlayerShootBehaviour : MonoBehaviour
    {
        [SerializeField] private GameObject _bulletPrefab;
        [SerializeField] private Transform _startBulletLocation;
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private LayerMask _shootScanLayerMask;

        [SerializeField] private float _bulletSpeed = 10;
        [SerializeField] private float _shootRaycastDistance = 500f;
        [SerializeField] private int _bulletStock = 10;
        [SerializeField] private float _shootCooldown = 0.5f;

        [SerializeField] private ParticleSystem _particleShot;
        [SerializeField] private ParticleSystem _particleAura;
        [SerializeField] private ParticleSystem _boltParticle;

        [SerializeField] private Animator _gunAnimator;

        private SoundManager _soundManager;
        private float _shootTimer = 0f;
        private GunSway _gunSway;
        private GameManager _gameManager;

        public int BulletStock => _bulletStock;
        public void RefillBullets(int amount)
        {
            _bulletStock += amount;
        }

        private void Awake()
        {
            GameObject soundManager = GameObject.Find("SoundManager");
            _gunSway = GetComponent<GunSway>();
            if (soundManager)
            {
                _soundManager = soundManager.GetComponent<SoundManager>();
            }

            _gameManager = FindAnyObjectByType<GameManager>();
        }

        private void Update()
        {
            if (_shootTimer > 0f)
            {
                _shootTimer -= Time.deltaTime;
            }
        }

        private void OnINeedMoreBullets(InputValue value)
        {
            _bulletStock += 100;
        }

        private void OnFire(InputValue value)
        {
            if (_gameManager.IsInPause) return;
            if (_gunAnimator.runtimeAnimatorController != null) return;
            if (_bulletStock <= 0) return;
            if (_shootTimer > 0f) return;
            _shootTimer = _shootCooldown;

            Vector3 targetPoint = GetShootTargetPoint();
            Vector3 startBulletPosition = _startBulletLocation.position;
            Vector3 startToTarget = (targetPoint - startBulletPosition).normalized;

            _gunSway.GunShot();
            _particleShot.Play();
            _particleAura.Play();
            _boltParticle.Play();

            GameObject bullet =
                Instantiate(_bulletPrefab, startBulletPosition, _startBulletLocation.rotation);
            if (bullet.TryGetComponent(out BulletBehaviour bulletBehaviour))
            {
                bulletBehaviour.Shoot(startToTarget, _bulletSpeed);
                if (_soundManager != null)
                    _soundManager.PlaySound(SoundManager.Sound.Shot);

                _bulletStock--;
            }
            else
            {
                Debug.LogError("Could not find BulletBehaviour on bullet prefab :thinking:");
            }
        }

        private Vector3 GetShootTargetPoint()
        {
            var rayStartPoint = new Vector3(Screen.width / 2f, Screen.height / 2f, 0);
            Ray ray = _mainCamera.ScreenPointToRay(rayStartPoint);

            if (Physics.Raycast(ray, out RaycastHit hit, _shootRaycastDistance, _shootScanLayerMask))
            {
                return hit.point;
            }

            return ray.origin + ray.direction * _shootRaycastDistance;
        }
    }
}