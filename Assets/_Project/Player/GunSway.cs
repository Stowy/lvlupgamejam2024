using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GunSway : MonoBehaviour
{
    Vector3 swayEulerRot = Vector3.zero;

    [SerializeField] private float swayStep = 0.01f;
    [SerializeField] private float swayMaxStep = 0.03f;
    [SerializeField] private float rotationStep = 0.25f;
    [SerializeField] private float maxRotationStep = 6f;
    [SerializeField] private GameObject _gun;

    private Vector3 swayPos;
    private Vector3 gunPos;
    private Quaternion gunRot;
    private Vector3 initialGunPos;
    private Quaternion initialGunRot;

    private Rigidbody _rigidBody;
    private GameManager _gameManager;

    private void Start()
    {
        _gameManager = FindAnyObjectByType<GameManager>();
        _rigidBody = GetComponent<Rigidbody>();
        initialGunPos = _gun.transform.localPosition;
        initialGunRot = _gun.transform.localRotation;
    }

    private void Update()
    {
        if (_gameManager.IsInPause) return;

        Vector2 mouseDelta = Mouse.current.delta.value;
        Vector3 invertLook = Vector3.zero;
        invertLook = mouseDelta * swayStep;
        invertLook += initialGunPos;
        invertLook.x = Mathf.Clamp(invertLook.x, initialGunPos.x - swayMaxStep, initialGunPos.x + swayMaxStep);
        invertLook.y = Mathf.Clamp(invertLook.y * 0.60f, initialGunPos.y - swayMaxStep, initialGunPos.y + swayMaxStep);
        invertLook.z = Mathf.Clamp(initialGunPos.z, initialGunPos.z - swayMaxStep, initialGunPos.z + swayMaxStep);
        swayPos = invertLook;

        invertLook = mouseDelta * rotationStep;
        invertLook.x = Mathf.Clamp(invertLook.x + initialGunRot.eulerAngles.y,
            initialGunRot.eulerAngles.y - maxRotationStep, initialGunRot.eulerAngles.y + maxRotationStep);
        invertLook.y = Mathf.Clamp(invertLook.y + initialGunRot.eulerAngles.x,
            initialGunRot.eulerAngles.x - maxRotationStep, initialGunRot.eulerAngles.x + maxRotationStep);
        invertLook.z = Mathf.Clamp(mouseDelta.x, -maxRotationStep, maxRotationStep);
        swayEulerRot = new Vector3(invertLook.y * 0.80f, invertLook.x, invertLook.z * 0.60f);

        _gun.transform.localPosition = Vector3.Lerp(gunPos, swayPos, Time.deltaTime * 5.0f);
        _gun.transform.localRotation = Quaternion.Slerp(gunRot, Quaternion.Euler(swayEulerRot), Time.deltaTime * 7.5f);
        gunPos = _gun.transform.localPosition;
        gunRot = _gun.transform.localRotation;
    }

    public void GunShot()
    {
        gunPos.z -= 0.20f;
    }
}