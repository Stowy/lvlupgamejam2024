﻿using System;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(PlayerShootBehaviour))]
    public class DeadBodyPicker : MonoBehaviour
    {
        [SerializeField] private Camera _mainCamera;
        [SerializeField] private GameObject _pickupMessage;
        [SerializeField] private GameObject _putScrapMessage;
        [SerializeField] private GameObject _getAmmoMessage;
        [SerializeField] private float _pickupDistance = 2;
        [SerializeField] private int _maxDeadBodies = 2;


        [SerializeField] private Animator _gunAnimator;
        [SerializeField] private RuntimeAnimatorController _gunAnimatorController;

        private GameObject _deadBody = null;
        private RecyclingStationBehaviour _recyclingStation = null;
        private PlayerShootBehaviour _playerShootBehaviour;
        private ScoreSystem _scoreSystem;
        private SoundManager _soundManager;

        private int _prevFib = 3;
        private int _nextFib = 5;
        private int _counterDeadBodies = 0;

        public void CheckUpgrade()
        {
            if (CheckFibonaci(_counterDeadBodies))
                _maxDeadBodies++;
        }

        bool CheckFibonaci(int current)
        {
            bool isUp = false;
            if (current >= _nextFib)
            {
                int sum = _prevFib + _nextFib;
                _prevFib = _nextFib;
                _nextFib = sum;
                isUp = true;
            }

            return isUp;
        }

        public int DeadBodyCount { get; private set; }

        public int MaxDeadBodies => _maxDeadBodies;

        public void ClearDeadBodies()
        {
            DeadBodyCount = 0;
        }

        private void Awake()
        {
            if (!_pickupMessage)
            {
                _pickupMessage = GameObject.Find("DeadBodyMessage");
                if (!_pickupMessage)
                {
                    Debug.LogError("Fréro stp donne moi le message du pickup");
                }
            }

            _playerShootBehaviour = GetComponent<PlayerShootBehaviour>();
        }

        private void Start()
        {
            _scoreSystem = FindFirstObjectByType<ScoreSystem>();
            _soundManager = FindAnyObjectByType<SoundManager>();
        }

        private void Update()
        {
            FindInteractables();
        }

        private void OnPickup()
        {
            if (_deadBody && !_recyclingStation)
            {
                if (DeadBodyCount >= _maxDeadBodies)
                {
                    Debug.Log("nope");
                    return;
                }

                _pickupMessage.SetActive(false);
                Destroy(_deadBody);
                if (_soundManager)
                    _soundManager.PlaySound(SoundManager.Sound.PickupScrap);
                DeadBodyCount++;
                _counterDeadBodies++;
            }
            else if (_recyclingStation && !_deadBody)
            {
                switch (_recyclingStation.State)
                {
                    case RecyclingStationBehaviour.RecyclingState.WaitingForScrap:
                        if (DeadBodyCount > 0)
                        {
                            _recyclingStation.FillScrap(DeadBodyCount);
                            if (_scoreSystem)
                                _scoreSystem.OnRecycle(DeadBodyCount);
                            DeadBodyCount = 0;
                            _putScrapMessage.SetActive(false);
                        }

                        break;
                    case RecyclingStationBehaviour.RecyclingState.WaitingForBulletPickup:
                    {
                        _gunAnimator.runtimeAnimatorController = _gunAnimatorController;
                        int ammoAmount = _recyclingStation.GetAmmo();
                        _playerShootBehaviour.RefillBullets(ammoAmount);
                        _getAmmoMessage.SetActive(false);
                        break;
                    }
                }
            }
        }

        private void FindInteractables()
        {
            // Désolé pour le code qui vas suivre...
            var rayDirection = new Vector3(Screen.width / 2f, Screen.height / 2f, 0);
            Ray ray = _mainCamera.ScreenPointToRay(rayDirection);
            if (Physics.Raycast(ray, out RaycastHit hit, _pickupDistance))
            {
                if (hit.collider.CompareTag("DeadEnemy"))
                {
                    _deadBody = hit.collider.gameObject;
                    _pickupMessage.SetActive(true);
                }
                else if (hit.collider.CompareTag("RecyclingStation"))
                {
                    _deadBody = null;
                    _pickupMessage.SetActive(false);

                    if (!hit.collider.TryGetComponent(out RecyclingStationBehaviour recyclingStation)) return;

                    _recyclingStation = recyclingStation;
                    switch (_recyclingStation.State)
                    {
                        case RecyclingStationBehaviour.RecyclingState.WaitingForScrap:
                            if (DeadBodyCount == 0) break;

                            _putScrapMessage.SetActive(true);
                            _getAmmoMessage.SetActive(false);
                            break;
                        case RecyclingStationBehaviour.RecyclingState.WaitingForBulletPickup:
                            _getAmmoMessage.SetActive(true);
                            _putScrapMessage.SetActive(false);
                            break;
                    }
                }
                else
                {
                    _pickupMessage.SetActive(false);
                    _deadBody = null;
                    _recyclingStation = null;
                    _putScrapMessage.SetActive(false);
                    _getAmmoMessage.SetActive(false);
                }
            }
            else
            {
                _pickupMessage.SetActive(false);
                _deadBody = null;
                _recyclingStation = null;
                _putScrapMessage.SetActive(false);
                _getAmmoMessage.SetActive(false);
            }
        }
    }
}