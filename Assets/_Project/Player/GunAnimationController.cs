using UnityEngine;

public class GunAnimationController : MonoBehaviour
{
    [SerializeField] private Animator _gunAnimator;
    [SerializeField] private RuntimeAnimatorController _gunAnimatorController;

    //private void OnReload(InputValue value)
    //{
    //    _gunAnimator.runtimeAnimatorController = _gunAnimatorController;
    //}

    public void EndAnimation()
    {
        _gunAnimator.runtimeAnimatorController = null;
    }
}