using System;
using UnityEngine;

namespace Player
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField] private int _maxHealth = 3;
        [SerializeField] private float _invulnerabilityTime = 0.5f;
        [SerializeField] private ScreenShake _screenshake;

        private float _invulnerabilityTimer = 0;

        private SoundManager _soundManager;

        public int Health { get; private set; }
        private bool IsDead => Health <= 0;

        public void Hurt()
        {
            if (!(_invulnerabilityTimer <= 0)) return;

            Health--;
            if (IsDead)
            {
                Die();
            }

            if (_soundManager)
            {
                _soundManager.PlaySound(SoundManager.Sound.Hurt);
                _soundManager.PlaySound(SoundManager.Sound.Rumble);
            }

            _screenshake.Screenshake();
            _invulnerabilityTimer = _invulnerabilityTime;
        }

        private void Awake()
        {
            Health = _maxHealth;

            GameObject soundManager = GameObject.Find("SoundManager");

            if (soundManager)
            {
                _soundManager = soundManager.GetComponent<SoundManager>();
            }
        }

        private void Update()
        {
            if (_invulnerabilityTimer > 0)
            {
                _invulnerabilityTimer -= Time.deltaTime;
            }
        }

        private void Die()
        {
            var levelLoader = FindFirstObjectByType<LevelLoader>();
            if (levelLoader)
                levelLoader.LoadScene("Score");
        }
    }
}