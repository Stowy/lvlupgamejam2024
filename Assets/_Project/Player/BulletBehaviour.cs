using System;
using System.Net;
using UnityEngine;

namespace Player
{
    public class BulletBehaviour : MonoBehaviour
    {
        [SerializeField] private float _damage = 5;

        private Rigidbody _rigidbody;

        public float Damage => _damage;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            Destroy(gameObject, 5);
        }

        public void Shoot(Vector3 direction, float speed)
        {
            _rigidbody.AddForce(direction * speed, ForceMode.Impulse);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy")) return;
            
            Destroy(gameObject);
        }
    }
}