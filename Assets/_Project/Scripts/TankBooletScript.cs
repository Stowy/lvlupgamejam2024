using Player;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TankBooletScript : MonoBehaviour
{
    // Start is called before the first frame update
    private void Awake()
    {
        Destroy(gameObject, 1);
        transform.localScale = Vector3.one * 0.2f;

        GameObject soundManager = GameObject.Find("SoundManager");
        if (soundManager)
        {
            soundManager.GetComponent<SoundManager>().PlaySound(SoundManager.Sound.Explosion);
        }
    }

    private void Update()
    {
        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(5f, 5f, 5f), Time.deltaTime*3);
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            playerHealth.Hurt();
            Debug.Log("BOOLET");
        }
    }
}
