using System;
using Player;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class EnemyTankBehaviour : MonoBehaviour
{
    public Transform _player;
    [SerializeField] private float _life = 15;
    [SerializeField] private GameObject _deadBodyPrefab;
    [SerializeField] private Transform _tankCrosshair;
    [SerializeField] private GameObject _tankBoolet;
    [SerializeField] private float _rotationSpeed = 180f;
    [SerializeField] private float _timeBeforeShooting = 5.0f;
    [SerializeField] private float _speed = 75;
    [SerializeField] private GameObject _antenna;

    private Vector3 lockPosition;
    private Quaternion lockRotation;

    private Animator _anim;
    private static readonly int IsHurting = Animator.StringToHash("isHurting");
    private ParticleSystem _particleSystem;
    private ScoreSystem _scoreSystem;
    private bool _lockedPosition = false;
    private bool _hasShot = false;
    private float _beenAliveTimer = 0.0f;

    private Rigidbody _rigidbody;
    private Vector3 _target;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _scoreSystem = FindFirstObjectByType<ScoreSystem>();
    }

    private void Update()
    {
        _beenAliveTimer += Time.deltaTime;
        if (_beenAliveTimer < _timeBeforeShooting && !_lockedPosition)
        {
            _target = -_player.position;
        }
        else
        {
            if (!_lockedPosition)
            {
                _lockedPosition = true;
                _beenAliveTimer = 0;
                _tankCrosshair.rotation = Quaternion.Euler(0, 0, 0);
                lockPosition = transform.position;
            }
        }

        if (_lockedPosition && _beenAliveTimer < 5f)
        {
            _tankCrosshair.position = _player.position + Vector3.down;
            _tankCrosshair.rotation *= Quaternion.Euler(0, _rotationSpeed * Time.deltaTime, 0);
            lockRotation = transform.rotation;
            transform.position = lockPosition;
            _antenna.transform.rotation *= Quaternion.Euler(0, _rotationSpeed * Time.deltaTime, 0);
        }

        if (_lockedPosition && _beenAliveTimer is > 5f and < 7f)
        {
            transform.rotation = lockRotation;
            transform.position = lockPosition;
            _antenna.transform.rotation *= Quaternion.Euler(0, _rotationSpeed * 2 * Time.deltaTime, 0);
            //_tankCrosshair.position = _tankCrosshair.position;
            //_tankCrosshair.rotation *= Quaternion.Euler(0, _rotationSpeed * Time.deltaTime, 0);
        }

        if (_lockedPosition && _beenAliveTimer > 7f && !_hasShot)
        {
            _hasShot = true;
            _tankCrosshair.rotation = Quaternion.Euler(0, 0, 180f);
            Instantiate(_tankBoolet, _tankCrosshair.position, _tankCrosshair.rotation);
            _antenna.transform.rotation = Quaternion.Euler(0, 4, 0);
        }

        if (_lockedPosition && _beenAliveTimer > 7f && _hasShot)
        {
            _target = -_player.position;
        }

        if (_lockedPosition && _beenAliveTimer > 15f)
        {
            lockPosition = transform.position;
            _beenAliveTimer = 0;
            _hasShot = false;
            _tankCrosshair.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void FixedUpdate()
    {
        Vector3 target = _player.position - transform.position;
        target.y = 0f;
        target.Normalize();

        transform.forward = target;
        _rigidbody.velocity = target * (_speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider.TryGetComponent(out PlayerHealth playerHealth)) return;
        playerHealth.Hurt();
    }

    private void OnTriggerEnter(Collider other)
    {
        HandleBulletCollision(other);
    }

    private void HandleBulletCollision(Component other)
    {
        if (!other.TryGetComponent(out BulletBehaviour bulletBehaviour)) return;
        _anim.SetTrigger(IsHurting);
        _particleSystem.Play();
        _life -= bulletBehaviour.Damage;

        Destroy(bulletBehaviour.gameObject);
        if (_life <= 0f)
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(true);

            Die();
        }
        else
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(false);
        }
    }

    private void Die()
    {
        Transform myTransform = transform;
        Instantiate(_deadBodyPrefab, myTransform.position, myTransform.rotation);
        Destroy(gameObject);
    }
}