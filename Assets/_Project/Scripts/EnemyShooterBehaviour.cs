using System;
using Player;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShooterBehaviour : MonoBehaviour
{
    public Transform _player;

    [SerializeField] private float _life = 15;
    [SerializeField] private GameObject _deadBodyPrefab;
    [SerializeField] private Transform _robotHead;
    [SerializeField] private ParticleSystem _particleRight;
    [SerializeField] private ParticleSystem _particleLeft;
    [SerializeField] private Transform _locationRight;
    [SerializeField] private Transform _locationLeft;
    [SerializeField] private GameObject _turretBoolet;
    [SerializeField] private float _speed = 75;
    [SerializeField] private float _timeBeforeShooting = 5.0f;

    private Animator _anim;
    private static readonly int IsHurting = Animator.StringToHash("isHurting");
    private ParticleSystem _particleSystem;
    private ScoreSystem _scoreSystem;
    private Rigidbody _rigidbody;

    private bool _hasShotRight = false;
    private bool _hasShotLeft = false;
    private bool _loadRight = false;

    private float _beenAliveTimer = 0.0f;
    private Vector3 _target;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _scoreSystem = FindFirstObjectByType<ScoreSystem>();
    }

    private void Update()
    {
        _beenAliveTimer += Time.deltaTime;
        _robotHead.transform.LookAt(_player);
        _target = _player.position + new Vector3(20, 0, 0);

        if (_beenAliveTimer > _timeBeforeShooting && !_loadRight)
        {
            _particleRight.Play();
            _loadRight = true;
        }

        if (_beenAliveTimer > _timeBeforeShooting + 1.5f && !_hasShotRight)
        {
            Instantiate(_turretBoolet, _locationRight.position, _locationRight.rotation);
            _particleRight.Stop();
            _particleLeft.Play();
            _hasShotRight = true;
        }

        if (_beenAliveTimer > _timeBeforeShooting + 3f && !_hasShotLeft)
        {
            Instantiate(_turretBoolet, _locationLeft.position, _locationLeft.rotation);
            _particleLeft.Stop();
            _hasShotLeft = true;
            _hasShotLeft = false;
            _hasShotRight = false;
            _loadRight = false;
            _beenAliveTimer = 0;
        }
    }

    private void FixedUpdate()
    {
        Vector3 target = _player.position - transform.position;
        target.y = 0f;
        target.Normalize();

        transform.forward = target;
        _rigidbody.velocity = target * (_speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider.TryGetComponent(out PlayerHealth playerHealth)) return;
        playerHealth.Hurt();
    }

    private void OnTriggerEnter(Collider other)
    {
        HandleBulletCollision(other);
    }

    private void HandleBulletCollision(Component other)
    {
        if (!other.TryGetComponent(out BulletBehaviour bulletBehaviour)) return;
        _anim.SetTrigger(IsHurting);
        _particleSystem.Play();
        _life -= bulletBehaviour.Damage;

        Destroy(bulletBehaviour.gameObject);
        if (_life <= 0f)
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(true);

            Die();
        }
        else
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(false);
        }
    }

    private void Die()
    {
        Transform myTransform = transform;
        Instantiate(_deadBodyPrefab, myTransform.position, myTransform.rotation);
        Destroy(gameObject);
    }
}