using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAudio : MonoBehaviour
{
    private SoundManager soundManager;
    // Start is called before the first frame update
    void Start()
    {
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }

    public void PlaySound(int sound)
    {
        soundManager.PlaySound((SoundManager.Sound)sound);
    }

    public void PlayMusic(int music)
    {
        soundManager.PlayMusic((SoundManager.Music)music);
    }
}
