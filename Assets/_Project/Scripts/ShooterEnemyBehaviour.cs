using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class ShooterEnemyBehaviour : MonoBehaviour
{
    public Transform _player;
    public Transform _robotHead;
    private NavMeshAgent _agent;
    // Start is called before the first frame update
    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update()
    {
        _robotHead.transform.LookAt(_player);
        _agent.destination = _player.position + new Vector3(20, 0, 0);
        //if (_agent.transform.position.magnitude - _player.transform.position.magnitude <= -10f)
        //{
        //    _agent.destination = _player.position;
        //}
        //else
        //{ 
        //    _agent.destination = -_player.position;
        //}
    }
}
