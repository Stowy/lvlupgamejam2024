using Player;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public Transform _player;
    [SerializeField] private float _life = 15;
    [SerializeField] private GameObject _deadBodyPrefab;
    [SerializeField] private float _speed = 75;

    private Animator _anim;
    private static readonly int IsHurting = Animator.StringToHash("isHurting");
    private ParticleSystem _particleSystem;
    private ScoreSystem _scoreSystem;
    private Rigidbody _rigidbody;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _particleSystem = GetComponentInChildren<ParticleSystem>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _scoreSystem = FindFirstObjectByType<ScoreSystem>();
    }

    private void FixedUpdate()
    {
        Vector3 target = _player.position - transform.position;
        target.y = 0f;
        target.Normalize();

        transform.forward = target;
        _rigidbody.velocity = target * (_speed * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.collider.TryGetComponent(out PlayerHealth playerHealth)) return;
        playerHealth.Hurt();
    }

    private void OnTriggerEnter(Collider other)
    {
        HandleBulletCollision(other);
    }

    private void HandleBulletCollision(Component other)
    {
        if (!other.TryGetComponent(out BulletBehaviour bulletBehaviour)) return;
        _anim.SetTrigger(IsHurting);
        _particleSystem.Play();
        _life -= bulletBehaviour.Damage;

        Destroy(bulletBehaviour.gameObject);
        if (_life <= 0f)
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(true);

            Die();
        }
        else
        {
            if (_scoreSystem)
                _scoreSystem.OnEnemyShoot(false);
        }
    }

    private void Die()
    {
        Transform myTransform = transform;
        Instantiate(_deadBodyPrefab, myTransform.position, myTransform.rotation);
        Destroy(gameObject);
    }
}