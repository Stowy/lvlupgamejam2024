using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorController : MonoBehaviour
{
    [SerializeField] private float velocity = 2.5f;
    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Rotate(0, velocity, 0);
    }
}
