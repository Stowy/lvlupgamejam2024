using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TrainAnimation : MonoBehaviour
{
    [FormerlySerializedAs("timeBeforeLoop")] [SerializeField]
    private float _timeBeforeLoop = 30f;

    [FormerlySerializedAs("speed")] [SerializeField]
    private float _speed = 10f;

    private float _timer;

    private void Update()
    {
        _timer += Time.deltaTime;
        Transform myTransform = transform;
        myTransform.position += new Vector3(_speed * Time.deltaTime, 0, 0);
        if (!(_timer > _timeBeforeLoop)) return;

        Vector3 position = myTransform.position;
        position = new Vector3(-53, position.y, position.z);
        myTransform.position = position;
        _timer = 0;
    }
}