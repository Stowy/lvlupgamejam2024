using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimplecrtMoveScript : MonoBehaviour
{

    [SerializeField] private RawImage _img1;
    [SerializeField] private RawImage _img2;

    void Update()
    {
        MoveUp(_img1);
        MoveUp(_img2);
    }
    private void MoveUp(RawImage img)
    {
        img.transform.localPosition -= new Vector3(0, 15*Time.deltaTime,0);
        if(img.transform.localPosition.y <= -250)
        {
            img.transform.localPosition -= new Vector3(0, 675, 0);
        }
    }
}
