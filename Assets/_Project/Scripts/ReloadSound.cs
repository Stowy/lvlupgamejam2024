using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadSound : MonoBehaviour
{

    private SoundManager _soundManager;
    // Start is called before the first frame update
    void Awake()
    {
        GameObject soundManager = GameObject.Find("SoundManager");
        if (soundManager)
        {
            _soundManager = soundManager.GetComponent<SoundManager>();
        }
    }

   public void Play()
    {
        if (_soundManager != null)
        {
            _soundManager.PlaySound(SoundManager.Sound.Reload);
        }
    }
}
