using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInPause : MonoBehaviour
{
    GameManager _gameManager;
    private void Start()
    {
        GameObject gameManager = GameObject.Find("GameManager");
        if (gameManager)
        {
            _gameManager = gameManager.GetComponent<GameManager>();
        }
    }

    public void Resume()
    {
        if(_gameManager)
            _gameManager.ResumeGame();
    }

    public void MainMenu()
    {
        Time.timeScale = 1f;
        Destroy(gameObject, 0.25f);
        GameObject.Find("LevelLoader").GetComponent<LevelLoader>().ButtonLoadScene("MainMenu");
        
    }
}
