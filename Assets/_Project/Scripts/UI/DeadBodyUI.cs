﻿using System;
using Player;
using TMPro;
using UnityEngine;

public class DeadBodyUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private DeadBodyPicker _bodyPicker;

    private void Awake()
    {
        if (!_bodyPicker)
        {
            _bodyPicker = FindFirstObjectByType<DeadBodyPicker>();
        }
    }

    private void Update()
    {
        // Yes I know it's slow on update....
        // who cares

        _text.text = $"Scrap: {_bodyPicker.DeadBodyCount}/{_bodyPicker.MaxDeadBodies}";
    }
}