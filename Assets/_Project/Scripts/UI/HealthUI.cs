﻿using System;
using Player;
using TMPro;
using UnityEngine;

public class HealthUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private PlayerHealth _playerHealth;

    private void Update()
    {
        _text.text = $"Health: {_playerHealth.Health}";
    }
}