using UnityEngine;
using TMPro;

public class IntroUI : MonoBehaviour
{
    [SerializeField] private GameObject _textIntro;
    [SerializeField] private RuntimeAnimatorController _animControlReady;
    [SerializeField] private RuntimeAnimatorController _animControl;

    private SoundManager _soundManager;

    private void Awake()
    {
        GameObject soundManager = GameObject.Find("SoundManager");
        if (soundManager)
        {
            _soundManager = soundManager.GetComponent<SoundManager>();
        }

        _textIntro.GetComponent<Animator>().runtimeAnimatorController = null;
    }

    public void LaunchIntro()
    {
        Ready();
    }

    private void Ready()
    {
        _textIntro.GetComponent<Animator>().runtimeAnimatorController = _animControlReady;
    }

    public void Go()
    {
        _textIntro.GetComponent<TextMeshProUGUI>().text = "Go!";
        _textIntro.GetComponent<Animator>().runtimeAnimatorController = _animControl;
    }

    public void EndIntro()
    {
        _textIntro.GetComponent<Animator>().runtimeAnimatorController = null;
        GetComponentInParent<Canvas>().enabled = false;
        GameObject gameManager = GameObject.Find("GameManager");
        if (gameManager != null)
            gameManager.GetComponent<GameManager>().LaunchParty();
    }

    public void VoiceReady()
    {
        if (_soundManager != null)
            _soundManager.PlayVoice(SoundManager.Voice.Ready);
    }

    public void VoiceGo()
    {
        if (_soundManager != null)
            _soundManager.PlayVoice(SoundManager.Voice.Go);
    }
}