﻿using System;
using Player;
using TMPro;
using UnityEngine;

public class AmmoUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _text;
    [SerializeField] private PlayerShootBehaviour _playerShootBehaviour;

    private void Update()
    {
        _text.text = $"Ammo: {_playerShootBehaviour.BulletStock}";
    }
}