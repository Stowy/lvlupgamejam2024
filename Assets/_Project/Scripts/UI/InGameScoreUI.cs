﻿using TMPro;
using UnityEngine;

namespace UI
{
    public class InGameScoreUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        private ScoreSystem _scoreSystem;

        private void Start()
        {
            _scoreSystem = FindFirstObjectByType<ScoreSystem>();
        }

        private void Update()
        {
            if (_scoreSystem)
                _text.text = $"{_scoreSystem.Score}";
        }
    }
}