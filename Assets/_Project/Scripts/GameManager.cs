using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [FormerlySerializedAs("canvasPause")] [Header("Parameters Menu")] [SerializeField]
    private Canvas _canvasPause;

    private SoundManager _soundManager;

    public bool IsInPause { get; set; }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        GameObject soundManager = GameObject.Find("SoundManager");

        if (soundManager)
        {
            _soundManager = soundManager.GetComponent<SoundManager>();
        }

        IsInPause = true;
    }

    public void PauseGame()
    {
        PauseMenu(true);
        IsInPause = true;
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
    }

    public void ResumeGame()
    {
        PauseMenu(false);
        IsInPause = false;

        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
    }

    private void PauseMenu(bool isVisible)
    {
        if (_soundManager != null)
            _soundManager.PlaySound(SoundManager.Sound.Button);

        GameObject canvas = GameObject.Find("CanvasPause(Clone)");
        if (!canvas)
        {
            Debug.LogWarning("No pause canvas here");
            return;
        }

        if (_canvasPause == null)
            _canvasPause = canvas.GetComponent<Canvas>();

        _canvasPause.enabled = isVisible;
    }

    public void OnPause()
    {
        if (!IsInPause)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    public void LaunchParty()
    {
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = true;
        IsInPause = false;
    }

    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus && !IsInPause)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}