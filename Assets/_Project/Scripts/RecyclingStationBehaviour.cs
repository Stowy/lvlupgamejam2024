using Player;
using System;
using UnityEngine;

public class RecyclingStationBehaviour : MonoBehaviour
{
    public enum RecyclingState
    {
        WaitingForScrap,
        Recycling,
        WaitingForBulletPickup
    }

    [SerializeField] private Material _waitingForScrapMaterial;
    [SerializeField] private Material _recyclingMaterial;
    [SerializeField] private Material _waitingForBulletMaterial;

    [SerializeField] private float _recyclingTime = 1f;

    [Tooltip(
        "If set to true, the recycling time will be multiplied by the amount of scrap. So if one scrap takes 1 second, two scraps will take two seconds.")]
    [SerializeField]
    private bool _multiplyRecyclingTimeByScrapCount = false;

    [SerializeField] private int _ammoPerScrap = 10;

    private RecyclingState _state = RecyclingState.WaitingForScrap;
    private int _scrapCount;
    private float _scrapTimer;
    private MeshRenderer _meshRenderer;
    private SoundManager _soundManager;

    private DeadBodyPicker _deadBodyPicker;

    public RecyclingState State => _state;

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
        _soundManager = FindAnyObjectByType<SoundManager>();

       _deadBodyPicker = GameObject.Find("Player").GetComponent<DeadBodyPicker>();
    }

    public void FillScrap(int scrapAmount)
    {
        if (_state != RecyclingState.WaitingForScrap)
        {
            Debug.LogError("You shouldn't fill scrap when the machine is not waiting for some");
            return;
        }

        if (scrapAmount <= 0)
        {
            Debug.LogWarning("Bruh you don't even have scrap");
            return;
        }

        _scrapCount = scrapAmount;

        if (_multiplyRecyclingTimeByScrapCount)
        {
            _scrapTimer = _recyclingTime * _scrapCount;
        }
        else
        {
            _scrapTimer = _recyclingTime;
        }

        _state = RecyclingState.Recycling;
        _meshRenderer.material = _recyclingMaterial;

        if (_soundManager)
            _soundManager.PlaySound(SoundManager.Sound.DropCorpse);
    }

    public int GetAmmo()
    {
        if (_state != RecyclingState.WaitingForBulletPickup)
        {
            Debug.LogError("pourquoi tu prends des balles quand la machine en a pas ????");
            return -1;
        }

        _state = RecyclingState.WaitingForScrap;
        _meshRenderer.material = _waitingForScrapMaterial;

        int ammoCount = _scrapCount * _ammoPerScrap;
        _scrapCount = 0;

        _deadBodyPicker.CheckUpgrade();
        
        if(_soundManager)
            _soundManager.PlaySound(SoundManager.Sound.CollecAmmunition);

        return ammoCount;
    }

    private void Update()
    {
        if (_state != RecyclingState.Recycling) return;

        if (_scrapTimer <= 0f)
        {
            _state = RecyclingState.WaitingForBulletPickup;
            _meshRenderer.material = _waitingForBulletMaterial;
            if (_soundManager)
                _soundManager.PlaySound(SoundManager.Sound.RecyclingDone);
        }
        else
        {
            _scrapTimer -= Time.deltaTime;
        }
    }
}