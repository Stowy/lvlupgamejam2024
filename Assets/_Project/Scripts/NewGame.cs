using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject canvasIntro = GameObject.Find("Text_Intro");
        if(canvasIntro != null)
            canvasIntro.GetComponent<IntroUI>().LaunchIntro();
        Destroy(gameObject, 4.0f);
    }

}
