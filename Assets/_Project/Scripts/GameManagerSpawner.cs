﻿using UnityEngine;

public class GameManagerSpawner : MonoBehaviour
{
    [SerializeField] private GameManager _gameManagerPrefab;

    private void Awake()
    {
        var gameManager = FindAnyObjectByType<GameManager>();
        if (!gameManager)
        {
            gameManager = Instantiate(_gameManagerPrefab);
            gameManager.ResumeGame();
        }

        Destroy(gameObject);
    }
}