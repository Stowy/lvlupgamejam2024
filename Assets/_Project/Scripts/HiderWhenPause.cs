﻿using System;
using UnityEngine;

public class HiderWhenPause : MonoBehaviour
{
    private Canvas _canvas;
    private GameManager _gameManager;

    private void Awake()
    {
        _canvas = GetComponent<Canvas>();
    }

    private void Start()
    {
        _gameManager = FindAnyObjectByType<GameManager>();
    }

    private void Update()
    {
        _canvas.enabled = !_gameManager.IsInPause;
    }
}