﻿using UnityEngine;
using UnityEngine.Serialization;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private GameObject _bigEnemy;

    [FormerlySerializedAs("_runningEnemy")] [SerializeField]
    private GameObject _shooterEnemy;

    [SerializeField] private Transform _playerReference;

    [SerializeField] private float _startSpawnTimeout = 2f;

    [SerializeField] private float _maxSpawnTimeout = 0.3f;

    [SerializeField] private float _timeToMaxSpawnSpeed = 60f * 2f;
    [SerializeField] private float _timeToBigEnemy = 45f;
    [SerializeField] private float _timeToRunningEnemy = 60f + 30f;

    [SerializeField] private AnimationCurve _speedIncreaseCurve;

    private float _elapsedTime;
    private float _nextEnemyTimer;

    private void Awake()
    {
        _nextEnemyTimer = _startSpawnTimeout;
    }

    private void Update()
    {
        _elapsedTime += Time.deltaTime;
        float timePercentage = _elapsedTime / _timeToMaxSpawnSpeed;
        float speedPercentage = _speedIncreaseCurve.Evaluate(timePercentage);
        float speed = _startSpawnTimeout + speedPercentage * (_maxSpawnTimeout - _startSpawnTimeout);

        if (_nextEnemyTimer > 0f)
        {
            _nextEnemyTimer -= Time.deltaTime;
        }
        else
        {
            _nextEnemyTimer = speed;
            if (_elapsedTime < _timeToBigEnemy && _elapsedTime < _timeToRunningEnemy)
            {
                SpawnEnemy(_enemyPrefab);
            }
            else if (_elapsedTime >= _timeToBigEnemy && _elapsedTime < _timeToRunningEnemy)
            {
                SpawnEnemy(Random.Range(0f, 1f) > 0.5f ? _enemyPrefab : _bigEnemy);
            }
            else if (_elapsedTime >= _timeToBigEnemy && _elapsedTime >= _timeToRunningEnemy)
            {
                float pick = Random.Range(0f, 1f);
                switch (pick)
                {
                    case < 0.3333f:
                        SpawnEnemy(_enemyPrefab);
                        break;
                    case >= 0.3333f and < 0.6666f:
                        SpawnEnemy(_bigEnemy);
                        break;
                    case >= 0.6666f:
                        SpawnEnemy(_shooterEnemy);
                        break;
                }
            }
        }
    }

    private void SpawnEnemy(GameObject prefab)
    {
        Transform myTransform = transform;
        GameObject enemy = Instantiate(prefab, myTransform.position, myTransform.rotation);
        if (enemy.TryGetComponent(out EnemyBehaviour enemyBehaviour))
        {
            enemyBehaviour._player = _playerReference;
        }

        if (enemy.TryGetComponent(out EnemyTankBehaviour enemyTankBehaviour))
        {
            enemyTankBehaviour._player = _playerReference;
        }

        if (enemy.TryGetComponent(out EnemyShooterBehaviour enemyShooterBehaviour))
        {
            enemyShooterBehaviour._player = _playerReference;
        }
    }
}