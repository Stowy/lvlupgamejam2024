using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class InfiniteTrainAnimation : MonoBehaviour
{
    [FormerlySerializedAs("speed")] [SerializeField]
    private float _speed = 10f;

    [FormerlySerializedAs("spawnDistance")] [SerializeField]
    private float _spawnDistance = 10f;

    private float _timer;

    private void Update()
    {
        _timer += Time.deltaTime;
        Transform myTransform = transform;
        myTransform.position += new Vector3(_speed * Time.deltaTime, 0, 0);
        if (!(myTransform.position.x > _spawnDistance - _spawnDistance)) return;

        Vector3 position = myTransform.position;
        position = new Vector3(-_spawnDistance, position.y, position.z);
        myTransform.position = position;
        _timer = 0;
    }
}